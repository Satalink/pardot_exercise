<?php

define(ERR_INVALIDIN, 1001);
define(ERR_INVALIDINT, 1002);

class Prime {
  /**
   * @param type integer
   * @throws Exception
   * Return primary numbers less than param.
   */
  public function get_primes($x){
    $prime_nums = array();  
    try {
      if(!is_numeric($x)) {
        throw new Exception("INVALIDIN: Input must be a number.", ERR_INVALIDIN);
      } elseif ($x < 0){
        throw new Exception("INVALIDINT: Input must be a positive number", ERR_INVALIDINT);
      }
      for ($i=0; $i<$x; $i++){
        if ($this->isprime($i)){
          array_push($prime_nums, $i);
        }
      }
    } catch (Exception $ex) {
      print "Error(".$ex->getCode()."): ".$ex->getMessage()."\n"
            .$ex->getFile()." line:".$ex->getLine()."\n"
            .$ex->getTraceAsString();
    }
    return($prime_nums);
  }

  /**
   * @param type int
   * @return boolean
   */
  private function isprime($x){
    if ($x == 2) { return true; }
    if ($x < 2 or $x % 2 == 0) { return false; }
    for ($i=3; $i<=ceil(sqrt($x)); $i=$i+2){
      if ($x%$i == 0){ return false; }
    }
    return true;
  }
}
