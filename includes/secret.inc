<?php

class Secret {
  private $modifier;

  /**
   * Initialize Secret
   * set secret value
   */
  public function __construct() {
    $this->set_modifier();
  }
  
  /**
   * @param type int
   * @return type int
   */
  public function secret($i){
    $value = round(($i * $this->modifier),0);
    return($value);
  }

  /**
   * set secret value random double
   */
  private function set_modifier(){
    $this->modifier = round(log(rand(1,99)),2);
  }

  /**
   * @return type double
   */
  public function get_modifier(){
    return($this->modifier);
  }

  public function __destruct() {
    $this->modifier = null;
  }
}