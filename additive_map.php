<?php
#@version $Id: pardot.exercise.pkg,v 1.0 2014-05-31 19:50:27 neal.garrett Exp $;
include "includes/prime.inc";
include "includes/secret.inc";

/*
 * “You are given a function 'secret()' that accepts a single integer parameter 
 * and returns an integer. In your favorite programming language, write a 
 * command-line program that takes one command-line argument (a number) and 
 * determines if the secret() function is additive 
 * [secret(x+y) = secret(x) + secret(y)], 
 * for all combinations x and y, where x and y are all prime numbers less than 
 * the number passed via the command-line argument.  
 * 
 * Describe how to run your examples.”
 */

$secret = new Secret();
print "\n"
      . "input: $argv[1]\n"
      . "modifier: ".$secret->get_modifier()
      . "\n\n";


/* Get prime numbers less than input argv[1] */
$prime = new Prime();
$primes = $prime->get_primes($argv[1]);

/*
 * Loop through each combination of primes
 * Display == if is additive,
 * Display != if not additive.
 */
foreach ($primes as $x){
  foreach ($primes as $y) {
    if($y < $x) { continue; } // skip redundant combinations: 2+3, skip 3+2
    $op = "!=";
    if ($secret->secret($x+$y) == ($secret->secret($x)+$secret->secret($y))){
      $op = "==";
    }
    print "[secret($x+$y) $op secret($x) + secret($y)] => ["
        . $secret->secret($x+$y)
        . " $op "
        . $secret->secret($x)." + ".$secret->secret($y)
        . "]\n";
  }
}


