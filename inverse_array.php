<?php

// input: [a => 1, b => 3, 'bob' => 23, 3 => 1]
// output: [1 => [a, 3], 3 => b, 23 => 'bob']

$ar1 = array(a => 1, b => 3, 'bob' => 23, 3 => 1, 4 => 1);

$ar2 = array();
foreach ($ar1 as $key => $value){
  $ar2[$value][] = $key;
}

var_dump($ar2);

